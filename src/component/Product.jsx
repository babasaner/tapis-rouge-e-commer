import { FavoriteBorderOutlined, SearchOffOutlined, ShoppingCartOutlined} from '@mui/icons-material'
import React from 'react'
import styled from 'styled-components'


const Info = styled.div `
    opacity: 0;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left:0;
    background-color: rgba(0,0,0,0.5);
    z-index: 3;
    display: flex;
    align-items: center;
    justify-content: center;
`
const Container = styled.div`
  flex: 1;
 margin: 5px;
 min-width: 280px;
 height: 350px;
 display: flex;
 align-items: center;
 justify-content: center;
 background: #f5fbfd;
 position: relative;
 cursor: pointer;

 &:hover ${Info}{
opacity: 1;
transition: all 0.8s ease;
 }
`

const Circle = styled.div `
width: 200px;
height: 200px;
border-radius: 50%;
background: #fff;
position: absolute;
    
`
const Image = styled.img `
height: 75%;
mix-blend-mode: darken;
z-index: 2;
    
`



const Icon = styled.div `
    width:40px;
    height: 40px;
    border-radius: 50%;
    background: #fff;
    display: flex;
    justify-content: center;
    align-items: center;
    
    margin: 10px;

    &:hover{
        background-color: #e9f5f5;
        transform: scale(1.2);
        transition: all 0.8s ease;
    }


    `





const Title = styled.h1`
  color: #fff;
  margin: 20px;
`



const Product = ({item}) => {
  return (
    <Container>
        <Circle/>
         <Image src={item.img}/>
         <Info>
            <Icon>
                <ShoppingCartOutlined/>
            </Icon>

            <Icon>
                <SearchOffOutlined/>
            </Icon>

            <Icon>
                <FavoriteBorderOutlined/>
            </Icon>
         </Info>
    </Container>
  )
}

export default Product