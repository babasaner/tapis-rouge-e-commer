import { Search, ShoppingCartOutlined } from '@mui/icons-material'
import { Badge } from '@mui/material'
import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
height:60px;
`
const Wrapper = styled.div `
display: flex;
justify-content:space-between;
padding: 10px 20px;
align-items: center;
`

const Left = styled.div`

flex: 1;
display: flex;
align-items: center;

`

const Langue = styled.span`

  font-size:14px;
cursor: pointer;
display: flex;
`

const SearchContainer= styled.div`
border:1px solid lightgray;
display: flex;
align-items: center;
margin-left: 5px;
`


const Input = styled.input`

  border:none ;
`
const Logo = styled.h1`

font-weight: bold;
`

const Center = styled.div`
flex: 1;
text-align: center;

`


const Right = styled.div`
flex: 1;
display: flex;
gap: 20px;
justify-content: flex-end;
`

const MenuItem = styled.div`
font-size:14px;
cursor: pointer;
`
const Navbar = () => {
  return (
    <Container>
        <Wrapper>
          <Left>
            <Langue>
              Fr
              <SearchContainer>
              <Input/>
                <Search style={{color:"gray",fontSize:"16"}}/>
              </SearchContainer>
            </Langue>
            </Left>
          <Center><Logo>Rama Shop</Logo></Center>
          <Right>
            <MenuItem>Se connecter</MenuItem>
            <MenuItem>Créer un compte</MenuItem>
            <MenuItem>

            <Badge badgeContent={5} color="primary">
            <ShoppingCartOutlined />
            </Badge>
            </MenuItem>
            </Right>

        </Wrapper>
    </Container>
  )
}

export default Navbar