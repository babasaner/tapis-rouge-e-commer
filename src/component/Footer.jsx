import { Email, Facebook, Instagram, LinkedIn,Map,Phone,YouTube } from '@mui/icons-material'
import React from 'react'
import styled from 'styled-components'
import './footer.css'


const Container= styled.div `
    display: flex;

`

const Left= styled.div `
    flex: 1;
    display: flex;
    flex-direction: column;
    padding: 20px;
`

const Logo = styled.h1`
    font-weight: bold;
`
const Desc = styled.p`
    margin: 20px 0px;
`
const SocialContainer = styled.div`
    
`

const Title = styled.h3`
    margin-bottom: 30px;
`
const List = styled.ul`
margin: 0;
padding: 0;
list-style: none;
display: flex;
flex-wrap: wrap;
    
`

const ListItem = styled.li`
    width: 100%;
`

const SocialIcon = styled.div`
    border-radius:50%;
    color: teal;

    display: flex;
    gap: 10px;
`
const Center= styled.div `
    flex:1;
    padding: 20px;
`

const Right= styled.div `
    flex: 1;
    padding: 20px;
`
const Contact= styled.div `
    display: flex;
    align-items: center;
    margin-bottom: 20px;
`
const Footer = () => {
  return (
    <Container>
        <Left>
        <Logo>
            RAMA SHOP
        </Logo>
       <Desc>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
         Odio eos ipsum placeat consequatur a! Dicta, sed ipsam minus voluptatibus
          voluptatum numquam, sunt adipisci mollitia, aut soluta ut doloribus dignissimos rem.
       </Desc>

       <SocialContainer>
        <SocialIcon color='385999'>
            <Facebook/>
            <Instagram/>
            <LinkedIn/>
            <YouTube/>
        </SocialIcon>
       </SocialContainer>
        </Left>

        <Center>
        <Title>
            Liens Utiles
        </Title>
            <List>
                <ListItem>
                        Accueil
                </ListItem>

                <ListItem>
                        Panier
                </ListItem>

                <ListItem>
                        Catégories
                </ListItem>

                <ListItem>
                        Mes commandes
                </ListItem>

                <ListItem>
                        Wishlist
                </ListItem>

                <ListItem>
                        Mon compte
                </ListItem>
        </List>
        </Center>

        <Right>
        <Title>
            Contact
        </Title>

        <Contact>
           <Map style={{marginRight: "10px"}}/> Mariste, en face école Japonaise
        </Contact>
        <Contact>
        <Phone style={{marginRight: "10px"}}/>  +221 77 345 90 90
        </Contact>

        <Contact>
        <Email style={{marginRight: "10px"}}/>  contact@ramashop.com
        </Contact>
        </Right>
    </Container>
  )
}

export default Footer