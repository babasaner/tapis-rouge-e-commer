
import styled from 'styled-components'
import {  categories } from "../data";
import MesCategories from './MesCategories';

const Container = styled.div`
  display: flex;
  padding: 20px;
  justify-content: space-between;

`
const Categories = () => {

  

  return (
    <Container>{categories.map(item=>(
        <MesCategories item={item} key={item.id}/>
    ))}</Container>
  )
}

export default Categories