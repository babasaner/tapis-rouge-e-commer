import Homme from './images/homme.jpeg'
import Femme from './images/Femme.jpg'

export const sliderItems = [
    {
      id: 1,
      img: "https://orcatrend.com/19846-medium_default/set-de-2-sellettes-en-metal-noir-et-bois.jpg",
      title: "SUMMER SALE",
      desc: "DON'T COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.",
      bg: "f5fafd",
    },
    {
      id: 2,
      img: "https://orcatrend.com/19849-medium_default/set-de-2-tables-d-appoint-design-en-verre-et-metal.jpg",
      title: "AUTUMN COLLECTION",
      desc: "DON'T COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.",
      bg: "fcf1ed",
    },
    {
      id: 3,
      img: "https://orcatrend.com/19666-medium_default/lampe-de-table-ceramique-taupe-et-marron-33-cm.jpg",
      title: "LOUNGEWEAR LOVE",
      desc: "DON'T COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.",
      bg: "fbf0f4",
    },
  ];

  export const categories = [
    {
      id: 1,
      img: Homme,
      title: "Homme",
    },
    {
      id: 2,
      img: Femme,
      title: "Tendance Femme",
    },
    {
      id: 3,
      img: "https://images.pexels.com/photos/5480696/pexels-photo-5480696.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",
      title: "LIGHT JACKETS",
    },
  ];

  export const popularProducts = [
    {
      id:1,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/11/SEJOUR-2.png",
    },
    {
      id:2,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/09/SEJOUR.png",
    },
    {
      id:3,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/11/sejour-19.png",
    },
    {
      id:4,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/11/2-3.png",
    },
    {
      id:5,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/09/chaise-1.png",
    },
    {
      id:6,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/11/canapes.png",
    },
    {
      id:7,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/11/SEJOUR-1.png",
    },
    {
      id:8,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/10/3-3.png",
    },

    {
      id:9,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/11/Table.jpg",
    },
    {
      id:10,
      img:"https://prestigedeco.sovereign-studio.com/wp-content/uploads/2020/11/Meuble-tv.jpg",
    },
  ]